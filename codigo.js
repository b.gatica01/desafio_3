
var items = []

$(document).ready(function () {

    var productVue = new Vue({
        el: '#lista_productos',
        data: {
            items: items
        },
        methods: {
            deleteButton(i) {
                alert("Delete")
                this.items.splice(i, 1)
            },
            updateButton(i) {
                alert("Update")
                var name = getUsername()
                var description = getDescription()
                var price = getPrice()

                if (description && name && price) {

                    var datos = {
                        name: name,
                        description: description,
                        price: price
                    }
            
                    this.items.splice(i, 1, datos)
            
                } else {
                    alert("Ingrese todos los datos")
                }

            }
        }
    })

    $('#btn-add').click(() => {
        var name = getUsername()
        var description = getDescription()
        var price = getPrice()
        addProduct(name, description, price)
    })

    

})

addProduct = (username, description, price) => {

    if (description && username && price) {

        var datos = {
            username: username,
            description: description,
            price: price
        }

        items.push(datos)

    } else {
        alert("Ingrese todos los datos")
    }

}



getPrice = () => {

    var precio = $('#precio-id').val()

    if (precio.trim().length == 0) {
        return null
    } else {
        return precio
    }

}

getDescription = () => {

    var descripcion = $('#descripcion-id').val()

    if (descripcion.trim().length == 0) {
        return null
    } else {
        return descripcion
    }

}

getUsername = () => {

    var nombre = $('#nombre-id').val()

    if (nombre.trim().length == 0) {
        return null
    } else {
        return nombre
    }

}

